<h2>CERIcompiler</h2>
A simple compiler.
From : Pascal-like imperative LL(k) langage
To : 64 bit 80x86 assembly langage (AT&T)

**La dernière version du projet est dans le dossier TP8.**


*  <h3>Fonctionnalité ajoutée : Statement CASE</h3>

Le Statement CASE fonctionne comme une série de if then else imbriqués.

<h4>Grammaire associée</h4>
CaseStatement := "CASE" Expression "OF" CaseListElement {";" CaseListElement} "END" <BR/>
CaseListElement := CaseLabelList ":" Statement | Empty <BR/>
CaseLabelList := Number {"," Number} <BR/>
Empty := <BR/>

<h4>Exemple d'utilisation</h4>

    VAR i,j : INTEGER.
    i:=1;
    CASE i OF
      1: DISPLAY 5;
      2: DISPLAY 1;
    END;
générera dans l'exécution du programme assembleur

    5

*  <h3>Fonctionnalité ajoutée : Option DOWNTO en complément du DO dans Statement FOR</h3> 

Cette option permet d'exécuter le statement tant que la variable contrôlant l'itération est supérieure ou égale 
à l'expression de fin.<BR/>La valeur de la variable controlant l'itération est décrémenté de 1 à chaque exécution.<BR/>
Avec l'option TO le statement s'exécute tant que la variable contrôlant l'itération est inférieure ou égale à l'expression de fin.
la valeur de la variable est incrémentée de 1 à chaque itération. 

<h4>Grammaire associée</h4>
    ForStatement := "FOR" AssignementStatement ["TO"|"DOWNTO"] Expression "DO" Statement
    
<h4>Exemple d'utilisation</h4>

    FOR I:=5 DOWNTO 1 DO DISPLAY I;
générera dans l'exécution du programme assembleur

    54321

*  <h3>Fonctionnalité ajoutée :Initialisation du BOOLEAN</h3>

L'initialisation d'une variable de type booléen a été ajoutéeAutorisation de testBool:= 0 ou 1
Un booléen peut avoir les valeurs suivantes 0 pour FALSE, toute autre valeur pour TRUE.

<h4>Exemple d'utilisation</h4>

    var a: BOOLEEN.
    a:=1; 
    DISPLAY a.
générera dans l'exécution du programme assembleur  

    True
	
*  <h3>Fonctionnalité ajoutée : Gestion de l'opérateur NOT (représenté par '!')</h3>

Cet opérateur retourne le complémentaire de la valeur booléene transmise.
Le booléen a les valeurs suivantes 0 pour FALSE, toute autre valeur est TRUE.
!0 aura la valeur TRUE et !1 aura la valeur FALSE.

<h4>Grammaire associée</h4>
Factor := Number | Letter | "(" Expression ")"| "!" Factor

<h4>Exemple d'utilisation</h4>

    var a: BOOLEEN.
    a:=1; 
    DISPLAY !a.
générera dans l'exécution du programme assembleur  

    False

*  <h3>Fonctionnalité ajoutée : Statement DISPLAYLN</h3>

Le Statement DISPLAYLN permet d'éditer une expression avec passage à la ligne 
(la fonction DISPLAY de base permet d'éditer une fonction sans passer à la ligne) 

<h4>Grammaire associée</h4>
DisplayLnStatement := "DISPLAYLN" Expression
    
<h4>Exemple d'utilisation</h4>

    DISPLAYLN 1;
    DIPSLAYLN 2;
générera dans l'exécution du programme assembleur

    1
    2 
alors que

    DISPLAY 1;
    DIPSLAY 2;
génère 

    12

*  <h3>Grammaire mise en oeuvre</h3>

    Program := [VarDeclarationPart] StatementPart <BR/>
    VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "." <BR/>
    VarDeclaration := Ident {"," Ident} ":" Type <BR/>
    StatementPart := Statement {";" Statement} "." <BR/>
    Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement | DisplayStatement | DisplayLnStatement <BR/>
    AssignementStatement := Identifier ":=" Expression <BR/>
    
    CaseStatement := "CASE" Expression "OF" CaseListElement {";" CaseListElement} "END" <BR/>
    CaseListElement := CaseLabelList ":" Statement | Empty <BR/>
    CaseLabelList := Number {"," Number} <BR/>
    Empty := <BR/>

    IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ] <BR/>
    WhileStatement := "WHILE" Expression DO Statement <BR/>
    ForStatement := "FOR" AssignementStatement ["TO"|"DOWNTO"] Expression "DO" Statement <BR/>
    BlockStatement := "BEGINN" Statement { ";" Statement } "END" <BR/>
    DisplayStatement := "DISPLAY" Expression <BR/>
    DisplayLnStatement := "DISPLAYLN" Expression <BR/>
    Expression := SimpleExpression [RelationalOperator SimpleExpression] <BR/>
    SimpleExpression := Term {AdditiveOperator Term} <BR/>
    Term := Factor {MultiplicativeOperator Factor} <BR/>
    Factor := Number | Letter | "(" Expression ")"| "!" Factor <BR/>
    Number := {digit}+(\.{digit}+)? <BR/>
    Identifier := Letter {(Letter|Digit)} <BR/>

    AdditiveOperator := "+" | "-" | "||" <BR/>
    MultiplicativeOperator := "*" | "/" | "%" | "&&" <BR/>
    RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">=" <BR/>
    Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9" <BR/>
    Letter := "a"|...|"z" <BR/>
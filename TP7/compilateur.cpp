//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//  Mis à jour par Elodie Giry Groupe 2 - 31/03/2020 - CERI L2S4
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"

//----------------------------------------------------------------------
// Grammaire mise en oeuvre
//----------------------------------------------------------------------
// Program := [VarDeclarationPart] StatementPart
// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
// VarDeclaration := Ident {"," Ident} ":" Type
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement | Display
// DisplayStatement := "DISPLAY" Expression
// DisplayLnStatement := "DISPLAYLN" Expression
// AssignementStatement := Identifier ":=" Expression

// IfStatement := "IF" Expression "THEN" Statement ["ELSE" Statement]
// WhileStatement := "WHILE" Expression DO Statement
// ForStatement := "FOR" AssignementStatement ["TO"|"DOWNTO"] Expression "DO" Statement
// BlockStatement := "BEGINN" Statement { ";" Statement } "END"

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := {digit}+(\.{digit}+)?
// Identifier := Letter {(Letter|Digit)}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"

#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>
#include <map>

using namespace std;

enum TYPE {INTEGER, BOOLEAN, CHAR, DOUBLE};
//TP4 : INT BOOL
//TP7 : INT-> INTEGER, BOOL -> BOOLEAN
//      Ajout de CHAR, DOUBLE

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};

TOKEN current;				// Current token

FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

set<string> IdVariables;
map<string,TYPE> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}

void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Identifier := Letter {(Letter|Digit)}
TYPE Identifier(void){
	//On recherche le type de la variable, on affiche 1 erreur si elle n'a pas été déclarée
	TYPE type;
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	type=DeclaredVariables[lexer->YYText()];
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return type;
}

// Number := {digit}+(\.{digit}+)?   
TYPE Number(void){
	
	bool is_a_decimal=false;
	double d;					// 64-bit float
	unsigned int *i;			// pointer to a 32 bit unsigned int 
	string	number=lexer->YYText();

	// Dans le push on utilise 1 format différent pour INT et DOUBLE
	// Si le nombre contient un . il est de type DOUBLE sinon INT

	if(number.find(".")!=string::npos){ // type DOUBLE
		d=atof(lexer->YYText());
		i=(unsigned int *) &d; // i points to the const double
		//cout <<"\tpush $"<<*i<<"\t# Conversion of "<<d<<endl;
		// Is equivalent to : 
		cout <<"\tsubq $8,%rsp\t\t\t# allocate 8 bytes on stack's top"<<endl;
		cout <<"\tmovl	$"<<*i<<", (%rsp)\t\t# Conversion of "<<d<<" (32 bit high part)"<<endl;
		cout <<"\tmovl	$"<<*(i+1)<<", 4(%rsp)\t# Conversion of "<<d<<" (32 bit low part)"<<endl;
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else{ // type INT
		cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
}

TYPE Expression(void);			// Called by Term() and calls Term()

//TP7 Ajout de la fonction pour gérer 1 constante de 1 caractère
//écrit dans l'assembleur le push pour le cacractère
//charConst := '\'\\?.\''
TYPE CharConst(void){
	cout<<"\tmovq $0, %rax"<<endl;
	cout<<"\tmovb $"<<lexer->YYText()<<",%al"<<endl;
	cout<<"\tpush %rax\t\t\t# push a 64-bit version of "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return CHAR;
}

// Factor := Number | Letter | "(" Expression ")"| "!" Factor
TYPE Factor(void){
	TYPE returnType;
	
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		returnType=Expression();
		
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else {
		if (current==NUMBER)
			returnType=Number();
		else if (current==ID)
				returnType=Identifier();
		else if(current==CHARCONST)           //TP7 traitement CHAR
				returnType=CharConst();
		else
			Error("'(' ou chiffre ou lettre attendue");
	}
	return returnType;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void){
	TYPE returnType1;
	TYPE returnType2;
	OPMUL mulop;
	returnType1=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		returnType2=Factor();
		
		if(returnType1!=returnType2){
			Error("Opération multiplicative et Type de variables incompatibles");
		}
		
		//Si les variables sont de type double on utilise les registres flottants : fldl st(0) et st(1)
		//sinon on utilise les registres généraux : rax et rbx
		
		switch(mulop){
			case AND:
				if(returnType2!=BOOLEAN){
					Error("Type non booléen pour l'opérateur AND");
				}
				cout << "\tpop %rbx"<<endl;			// get first operand
				cout << "\tpop %rax"<<endl;			// get second operand
				cout << "\tmulq	%rbx"<<endl;	    // a * b -> %rdx:%rax
				cout << "\tpush %rax\t\t\t# AND"<<endl;	// store result
				break;
			case MUL:
				if(returnType2!=INTEGER&&returnType2!=DOUBLE)
					Error("type non numérique pour la multiplication");
				if(returnType2==INTEGER){
					cout << "\tpop %rbx"<<endl;		// get first operand
					cout << "\tpop %rax"<<endl;		// get second operand
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t\t\t# MUL"<<endl;	// store result
				}
				else{
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfmulp	%st(0),%st(1)\t\t\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t\t# result on stack's top"<<endl; 
				}
				break;
			case DIV:
				if(returnType2!=INTEGER&&returnType2!=DOUBLE)
					Error("type non numérique pour la division");
				if(returnType2==INTEGER){
					cout << "\tpop %rbx"<<endl;			// get first operand
					cout << "\tpop %rax"<<endl;			// get second operand
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t\t\t# DIV"<<endl;	// store result
				}
				else{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t\t\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfdivp	%st(0),%st(1)\t\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t\t# result on stack's top"<<endl; 
				}
				break;
			case MOD:
				if(returnType2!=INTEGER)
					Error("type non entier pour le modulo");
				cout << "\tpop %rbx"<<endl;			// get first operand
				cout << "\tpop %rax"<<endl;			// get second operand
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t\t\t# MOD"<<endl;	// store result
				break;
			default:
				Error("Opérateur multiplicatif attendu");
		}
	}
	return returnType1;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void){
	OPADD adop;
	TYPE returnType1;
	TYPE returnType2;
	returnType1=Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		returnType2=Term();
		
		if(returnType1!=returnType2){
			Error("Opération additive et Type de variables incompatible");
		}

		//Si les variables sont de type double on utilise les registres flottants : fldl st(0) et st(1)
		//sinon on utilise les registres généraux : rax et rbx

		switch(adop){
			case OR:
				if(returnType2!=BOOLEAN)
					Error("opérande non booléenne pour l'opérateur OR");
				cout << "\tpop %rbx"<<endl;				// get first operand
				cout << "\tpop %rax"<<endl;				// get second operand
				cout << "\torq	%rbx, %rax\t\t# OR"<<endl;// operand1 OR operand2
				cout << "\tpush %rax"<<endl;			// store result
				break;			
			case ADD:
				if(returnType2!=INTEGER&&returnType2!=DOUBLE)
					Error("opérande non numérique pour l'addition");
				if(returnType2==INTEGER){
					cout << "\tpop %rbx"<<endl;					// get first operand
					cout << "\tpop %rax"<<endl;					// get second operand
					cout << "\taddq	%rbx, %rax\t\t# ADD"<<endl;	// add both operands
					cout << "\tpush %rax"<<endl;				// store result
				}
				else{
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t\t\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfaddp	%st(0),%st(1)\t\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t\t# result on stack's top"<<endl; 
				}
				break;
			case SUB:	
				if(returnType2!=INTEGER&&returnType2!=DOUBLE)
					Error("opérande non numérique pour la soustraction");
				if(returnType2==INTEGER){
					cout << "\tpop %rbx"<<endl;					// get first operand
					cout << "\tpop %rax"<<endl;					// get second operand
					cout << "\tsubq	%rbx, %rax\t\t# ADD"<<endl;	// add both operands
					cout << "\tpush %rax"<<endl;				// store result
				}
				else{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t\t\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfsubp	%st(0),%st(1)\t\t# %st(0) <- op1 - op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t\t# result on stack's top"<<endl; 
				}
				break;	

			default:
				Error("opérateur additif inconnu");
		}
	}
	return returnType1;
}

// VarDeclaration := Ident {"," Ident} ":" Type
void VarDeclaration(void){
	
	if(current!=ID)
		Error("Un identificater était attendu");
	
	IdVariables.insert(lexer->YYText());
	
	current=(TOKEN) lexer->yylex();

	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		
		IdVariables.insert(lexer->YYText());
		
		current=(TOKEN) lexer->yylex();
	}
	if(strcmp(lexer->YYText(), ":")!=0)
		Error("caractère ':' attendu");
	current=(TOKEN) lexer->yylex();
	
	TYPE tmpType;
	if(strcmp(lexer->YYText(), "INTEGER")==0) {
		tmpType=INTEGER;
	}
	else if(strcmp(lexer->YYText(), "BOOLEAN")==0) {
		tmpType=BOOLEAN;
	}
	else if(strcmp(lexer->YYText(), "CHAR")==0) {
		tmpType=CHAR;
	}
	else if(strcmp(lexer->YYText(), "DOUBLE")==0) {
		tmpType=DOUBLE;
	}
	else{
		Error("Type de variable attendu");
	}

	//on a lu le type des variables stockées dans IdVariables,
	//on lit le tableau IdVarialbles et on écrit les variables et leur type dans DeclaredVariables
	for (set<string>::iterator i = IdVariables.begin(); i != IdVariables.end(); ++i)
    {
        DeclaredVariables[*i]=tmpType; //=type de la variable
        switch(tmpType){
			case INTEGER:
				cout << *i << ":\t.quad 0"<<endl;      //on ecrit la variable dans l'assembleur
				break;
			case BOOLEAN:
				cout << *i << ":\t.quad 0"<<endl;
				break;
			case CHAR:
				cout << *i << ":\t.byte 0"<<endl;
				break;
			case DOUBLE:
				cout << *i << ":\t.double 0.0"<<endl;
				break;
			default:
				break;
		}
    }
	IdVariables.clear(); 	//On réinitialise le tableau des variables
	current=(TOKEN) lexer->yylex();
}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void){
	
	if(strcmp(lexer->YYText(), "VAR")!=0)
		Error("caractères 'VAR' attendu");
	
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;

	cout << "FormatString1:\t.string \"%llu\"\t\t# used by printf to display 64-bit unsigned integers"<<endl; 
	cout << "FormatString1L:\t.string \"%llu\\n\"\t# used by printf to display 64-bit unsigned integers avec saut ligne"<<endl; 
	
	cout << "FormatString2:\t.string \"%lf\"\t\t# used by printf to display 64-bit floating point numbers"<<endl; 
	cout << "FormatString2L:\t.string \"%lf\\n\"\t\t# used by printf to display 64-bit floating point numbers avec saut de ligne"<<endl; 
	
	cout << "FormatString3:\t.string \"%c\"\t\t# used by printf to display a 8-bit single character"<<endl; 
	cout << "FormatString3L:\t.string \"%c\\n\"\t\t# used by printf to display a 8-bit single character avec saut de ligne"<<endl; 
	
	cout << "TrueString:\t.string \"TRUE\"\t\t# used by printf to display the boolean value TRUE"<<endl; 
	cout << "FalseString:\t.string \"FALSE\"\t\t# used by printf to display the boolean value FALSE"<<endl; 
	
	current=(TOKEN) lexer->yylex();
	
	VarDeclaration();
	
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		VarDeclaration();
	}
	if(current!=DOT){
		Error("'.' était attendu");
	}
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPE Expression(void){
	OPREL oprel;
	TYPE returnType1;
	TYPE returnType2;
	returnType1=SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		returnType2=SimpleExpression();
		
		if(returnType1!=returnType2){
			Error("Opération relationnelle et Type de variables incompatibles");
		}

		//Si les variables sont de type double on utilise les registres flottants : fldl st(0) et st(1)
		//sinon on utilise les registres généraux : rax et rbx
		
		if(returnType1==DOUBLE){
			cout<<"\tfldl	(%rsp)\t"<<endl;
			cout<<"\tfldl	8(%rsp)\t\t\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
			cout<<"\t addq $16, %rsp\t\t\t# 2x pop nothing"<<endl;
			cout<<"\tfcomip %st(1)\t\t\t# compare op1 and op2 -> %RFLAGS and pop"<<endl;
			cout<<"\tfaddp %st(1)\t\t\t# pop nothing"<<endl;
		}
		else{
			cout << "\tpop %rax"<<endl;
			cout << "\tpop %rbx"<<endl;
			cout << "\tcmpq %rax, %rbx"<<endl;
		}

		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t\t\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t\t\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t\t\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t\t\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t\t\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t\t\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		
		return BOOLEAN;
	}
	return returnType1;
}

// AssignementStatement := Identifier ":=" Expression
TYPE AssignementStatement(void){
	TYPE returnType1;
	TYPE returnType2;
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	returnType1=DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	returnType2=Expression();
	
	if(returnType1!=returnType2){
		if ((returnType1!=BOOLEAN) or (returnType2!=INTEGER)){
			Error("Type incompatible");
		}
	}
		
	cout << "\tpop "<<variable<<endl;
	return returnType1;
}

void Statement(void);

// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void){
	TYPE returnType;
	int tag;
	tag=++TagNumber;
	if(strcmp(lexer->YYText(), "IF")==0){
		cout << "IF"<<tag<<":"<<endl;
		current=(TOKEN) lexer->yylex();
		returnType=Expression();
		
		if(returnType!=BOOLEAN){
			Error("Type incompatible");
		}
		
		cout << "\tpop %rax"<<endl;
		cout << "\tcmpq $0, %rax"<<endl;
		cout << "\tje ELSE"<<tag<<endl;
		
		cout << "THEN"<<tag<<":"<<endl;
		if(strcmp(lexer->YYText(), "THEN")==0){
			current=(TOKEN) lexer->yylex();
			Statement();
			cout << "\tjmp FinSi"<<tag << endl;
			
			cout << "ELSE"<<tag<<":"<<endl;
			if(strcmp(lexer->YYText(), "ELSE")==0){
				current=(TOKEN) lexer->yylex();
				Statement();
			}
		cout<<"FinSi"<<tag<<":"<<endl;
		}
		else{
			Error("'THEN' attendu");
		}
	}
	else{
		Error("'IF' attendu");
	}
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t\t\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t\t\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// WhileStatement := "WHILE" Expression DO Statement
void WhileStatement(void){
	TYPE returnType;
	int tag;
	tag=++TagNumber;
	if(strcmp(lexer->YYText(), "WHILE")==0){
		cout << "WHILE"<<tag<<":"<<endl;
		current=(TOKEN) lexer->yylex();
		returnType=Expression();
		
		if(returnType!=BOOLEAN){
			Error("Type incompatible");
		}
		
		cout << "\tpop %rax"<<endl;
		cout << "\tcmpq $0, %rax"<<endl;
		cout << "\tje FinWhile"<<tag<<endl;
		cout << "DO"<<tag<<":"<<endl;
		if(strcmp(lexer->YYText(), "DO")==0){
			current=(TOKEN) lexer->yylex();
			Statement();
			cout << "\tjmp WHILE"<<tag<<endl;
		}
		else{
			Error("'DO' attendu");
		}
		cout << "FinWhile"<<tag<<":"<<endl;
	}
	else{
			Error("'WHILE' attendu");
	}
}

// ForStatement := "FOR" AssignementStatement ["TO"|"DOWNTO"] Expression "DO" Statement
void ForStatement(void){
	int tag, max;
	tag=++TagNumber;
	string variable;
	if(strcmp(lexer->YYText(), "FOR")==0){
		current=(TOKEN) lexer->yylex();
		cout << "FOR"<<tag<<":"<<endl;
		variable=lexer->YYText();
		AssignementStatement();
				
		if(strcmp(lexer->YYText(), "TO")==0){
			cout << "TO"<<tag<<":"<<endl;
			current=(TOKEN) lexer->yylex();
			max=current;
			Expression();
						
			cout << "DO"<<tag<<":"<<endl;
			if(strcmp(lexer->YYText(), "DO")==0){
				current=(TOKEN) lexer->yylex();
				cout << "\tpop %rbx" <<endl; //rbx = max
				cout << "\tmovq "<<variable<<", %rax"<<endl;
				cout << "\tcmpq %rbx, %rax"<<endl;
				cout << "\tja FinFor"<<tag<<endl;
				cout << "\tpush %rbx" <<endl;
				Statement();
				cout << "\tmovq "<<variable<<", %rax"<<endl;
				cout << "\taddq $1, %rax"<<endl;
				cout << "\tmovq %rax, "<<variable<<endl;
				cout << "\tjmp DO"<<tag<<endl;
			}
			else{
				Error("'DO' attendu");
			}
		}
		
		else if(strcmp(lexer->YYText(), "DOWNTO")==0){
			cout << "DOWNTO"<<tag<<":"<<endl;
			current=(TOKEN) lexer->yylex();
			max=current;
			Expression();
						
			cout << "DO"<<tag<<":"<<endl;
			if(strcmp(lexer->YYText(), "DO")==0){
				current=(TOKEN) lexer->yylex();
				cout << "\tpop %rbx" <<endl; //rbx = max
				cout << "\tmovq "<<variable<<", %rax"<<endl;
				cout << "\tcmpq %rbx, %rax"<<endl;
				cout << "\tjb FinFor"<<tag<<endl;
				cout << "\tpush %rbx" <<endl;
				Statement();
				cout << "\tmovq "<<variable<<", %rax"<<endl;
				cout << "\tsubq $1, %rax"<<endl;
				cout << "\tmovq %rax, "<<variable<<endl;
				cout << "\tjmp DO"<<tag<<endl;
			}
			else{
				Error("'DO' attendu");
			}
		}
		
		else{
			Error("'TO' ou 'DOWNTO' attendu");
		}
		cout << "FinFor"<<tag<<":"<<endl;
	}
	else{
			Error("'FOR' attendu");
	}
}

// BlockStatement := "BEGINN" Statement { ";" Statement } "END"
void BlockStatement(void){
	int tag;
	tag=++TagNumber;
	if(strcmp(lexer->YYText(), "BEGINN")==0){
		current=(TOKEN) lexer->yylex();
		cout << "BEGINN"<<tag<<":"<<endl;
		Statement();
		while(strcmp(lexer->YYText(), ";")==0){
			current=(TOKEN) lexer->yylex();
			Statement();
		}
		if(strcmp(lexer->YYText(), "END")==0){
			current=(TOKEN) lexer->yylex();
			cout << "END"<<tag<<":"<<endl;
		}
		else{
			Error("'END' attendu");
		}
		cout << "FinBeginn"<<tag<<":"<<endl;
	}
	else{
			Error("'BEGINN' attendu");
		}
}

// DisplayStatement := "DISPLAY" Expression
void DisplayStatement(void){
	int tag;
	tag=++TagNumber;
	if(strcmp(lexer->YYText(), "DISPLAY")==0){
		current=(TOKEN) lexer->yylex();
		cout << "DISPLAY"<<tag<<":"<<endl;
		
		switch(Expression()){
			case INTEGER:
				cout << "\tpop %rsi\t\t\t# Affichage entier"<<endl;
				cout << "\tmovq $FormatString1, %rdi"<<endl;  //\t# \"%llu\\n\"
				cout << "\tmovl	$0, %eax"<<endl;
				cout << "\tcall	printf@PLT"<<endl;
				break;
			case BOOLEAN:
				cout << "\tpop %rdx\t\t\t# Affichage booleen"<<endl;
				cout << "\tcmpq $0, %rdx"<<endl;
				cout << "\tje False"<<tag<<endl;
				cout << "\tmovq $TrueString, %rdi\t\t# \"TRUE\""<<endl;
				cout << "\tjmp Next"<<tag<<endl;
				cout << "False"<<tag<<":"<<endl;
				cout << "\tmovq $FalseString, %rdi\t\t# \"FALSE\""<<endl;
				cout << "Next"<<tag<<":"<<endl;
				cout << "\tcall	puts@PLT"<<endl;
				break;
			case DOUBLE:
				cout << "\tmovsd	(%rsp), %xmm0\t\t# Affichage double &stack top -> %xmm0"<<endl;
				cout << "\tsubq	$16, %rsp\t\t# allocation for 3 additional doubles"<<endl;
				cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
				cout << "\tmovq $FormatString2, %rdi"<<endl;
				cout << "\tmovq	$1, %rax"<<endl;
				cout << "\tcall	printf@PLT"<<endl;
				cout << "nop"<<endl;
				cout << "\taddq $24, %rsp\t\t\t# pop nothing"<<endl;
				break;
			case CHAR:
				cout<<"\tpop %rsi\t\t\t# Affichage char (8 lowest bits of %si"<<endl;
				cout << "\tmovq $FormatString3, %rdi"<<endl;
				cout << "\tmovl	$0, %eax"<<endl;
				cout << "\tcall	printf@PLT"<<endl;
				break;
			default:
				Error("DISPLAY ne fonctionne pas pour ce type de donnée.");
		}
	}
	else{
			Error("'DISPLAY' attendu");	
	}
}

// DisplayStatement := "DISPLAYLN" Expression
void DisplayLnStatement(void){
	int tag;
	tag=++TagNumber;
	if(strcmp(lexer->YYText(), "DISPLAYLN")==0){
		current=(TOKEN) lexer->yylex();
		cout << "DISPLAYLN"<<tag<<":"<<endl;
		
		switch(Expression()){
			case INTEGER:
				cout << "\tpop %rsi\t\t\t# Affichage entier"<<endl;
				cout << "\tmovq $FormatString1L, %rdi"<<endl;  
				cout << "\tmovl	$0, %eax"<<endl;
				cout << "\tcall	printf@PLT"<<endl;
				break;
			case BOOLEAN:
				cout << "\tpop %rdx\t\t\t# Affichage booleen"<<endl;
				cout << "\tcmpq $0, %rdx"<<endl;
				cout << "\tje False"<<tag<<endl;
				cout << "\tmovq $TrueString, %rdi\t\t# \"TRUE\""<<endl;
				cout << "\tjmp Next"<<tag<<endl;
				cout << "False"<<tag<<":"<<endl;
				cout << "\tmovq $FalseString, %rdi\t\t# \"FALSE\""<<endl;
				cout << "Next"<<tag<<":"<<endl;
				cout << "\tcall	puts@PLT"<<endl;
				break;
			case DOUBLE:
				cout << "\tmovsd	(%rsp), %xmm0\t\t# Affichage double &stack top -> %xmm0"<<endl;
				cout << "\tsubq	$16, %rsp\t\t# allocation for 3 additional doubles"<<endl;
				cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
				cout << "\tmovq $FormatString2L, %rdi"<<endl;
				cout << "\tmovq	$1, %rax"<<endl;
				cout << "\tcall	printf@PLT"<<endl;
				cout << "nop"<<endl;
				cout << "\taddq $24, %rsp\t\t\t# pop nothing"<<endl;
				break;
			case CHAR:
				cout<<"\tpop %rsi\t\t\t# Affichage char (8 lowest bits of %si"<<endl;
				cout << "\tmovq $FormatString3L, %rdi"<<endl;
				cout << "\tmovl	$0, %eax"<<endl;
				cout << "\tcall	printf@PLT"<<endl;
				break;
			default:
				Error("DISPLAYLN ne fonctionne pas pour ce type de donnée.");
		}
	}
	else{
			Error("'DISPLAYLN' attendu");	
	}
}

// Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement | DisplayStatement | DisplayLnStatement
void Statement(void){
	
	if(strcmp(lexer->YYText(), "IF")==0) IfStatement();
	else if(strcmp(lexer->YYText(), "WHILE")==0) WhileStatement();
	else if(strcmp(lexer->YYText(), "FOR")==0) ForStatement();
	else if(strcmp(lexer->YYText(), "BEGINN")==0) BlockStatement(); 
	else if(strcmp(lexer->YYText(), "DISPLAY")==0) DisplayStatement();
	else if(strcmp(lexer->YYText(), "DISPLAYLN")==0) DisplayLnStatement();  
	else AssignementStatement();
}

// Program := [VarDeclarationPart] StatementPart
void Program(void){
	if(strcmp(lexer->YYText(), "VAR")==0){
		VarDeclarationPart();
	}
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}
}

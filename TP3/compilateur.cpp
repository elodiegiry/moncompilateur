//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>
#include <map>

using namespace std;

enum TYPE {INT, BOOL};    //TP4 ajout

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};


TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
//set<string> DeclaredVariables;
map<string,TYPE> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

//TP4
//Expression doit maintenant renvoyer un BOOL ou INT
//Tous les autres: INT

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"

//AJOUT DE DISPLAY TP5
//Statement := AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement |Display
//IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
//WhileStatement := "WHILE" Expression DO Statement
//ForStatement := "FOR" AssignementStatement "To" Expression "DO" Statement
//BlockStatement := "BEGIN" Statement { ";" Statement } "END"
	
		
//POUR L'INSTANT INT, PLUS TARD LE VRAI TYPE
TYPE Identifier(void){
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return INT;
}

TYPE Number(void){
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	return INT;
}

TYPE Expression(void);			// Called by Term() and calls Term()

TYPE Factor(void){
	TYPE returnType;
	
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		returnType=Expression();
		
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else 
		if (current==NUMBER)
			returnType=Number();
			
	     	else
				if(current==ID)
					returnType=Identifier();
					
				else
					Error("'(' ou chiffre ou lettre attendue");
					
	return returnType;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void){
	TYPE returnType1;
	TYPE returnType2;
	OPMUL mulop;
	returnType1=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		returnType2=Factor();
		
		if(returnType1!=returnType2){
			Error("Type incompatible");
		}
		
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
				break;
			case DIV:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				break;
			case MOD:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return returnType1;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void){
	OPADD adop;
	TYPE returnType1;
	TYPE returnType2;
	returnType1=Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		returnType2=Term();
		
		if(returnType1!=returnType2){
			Error("Type incompatible");
		}
		
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;			
			case SUB:	
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return returnType1;

}

// DeclarationPart := "[" Ident {"," Ident} "]"
void DeclarationPart(void){
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	
	cout <<"FormatString1:    .string \"\%llu\\n\"" <<endl; //used by printf to display 64-bit unsigned integers
	
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	//DeclaredVariables.insert(lexer->YYText());
	DeclaredVariables[lexer->YYText()]=INT;
	current=(TOKEN) lexer->yylex();
	
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		//DeclaredVariables.insert(lexer->YYText());
		DeclaredVariables[lexer->YYText()]=INT;
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPE Expression(void){
	OPREL oprel;
	TYPE returnType1;
	TYPE returnType2;
	returnType1=SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		returnType2=SimpleExpression();
		
		if(returnType1!=returnType2){
			Error("Type incompatible");
		}
		
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		
		return BOOL;
	}
	
	return returnType1;
}

// AssignementStatement := Identifier ":=" Expression
TYPE AssignementStatement(void){
	TYPE returnType1;
	TYPE returnType2;
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	returnType1=DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	returnType2=Expression();
	
	if(returnType1!=returnType2){
		Error("Type incompatible");
	}
	
	cout << "\tpop "<<variable<<endl;

	return returnType1;
}

void Statement(void);

void IfStatement(void){
	TYPE returnType;
	int tag;
	tag=++TagNumber;
	if(strcmp(lexer->YYText(), "IF")==0){
		cout << "IF"<<tag<<":"<<endl;
		current=(TOKEN) lexer->yylex();
		returnType=Expression();
		
		if(returnType!=BOOL){
			Error("Type incompatible");
		}
		
		cout << "\tpop %rax"<<endl;
		cout << "\tcmpq $0, %rax"<<endl;
		cout << "\tje ELSE"<<tag<<endl;
		
		cout << "THEN"<<tag<<":"<<endl;
		if(strcmp(lexer->YYText(), "THEN")==0){
			current=(TOKEN) lexer->yylex();
			Statement();
			cout << "\tjmp FinSi"<<tag << endl;
			
			cout << "ELSE"<<tag<<":"<<endl;
			if(strcmp(lexer->YYText(), "ELSE")==0){
				current=(TOKEN) lexer->yylex();
				Statement();
			}
		cout<<"FinSi"<<tag<<":"<<endl;
		}
		else{
			Error("'THEN' attendu");
		}
	}
	else{
		Error("'IF' attendu");
	}
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}


void WhileStatement(void){
	TYPE returnType;
	int tag;
	tag=++TagNumber;
	if(strcmp(lexer->YYText(), "WHILE")==0){
		cout << "WHILE"<<tag<<":"<<endl;
		current=(TOKEN) lexer->yylex();
		returnType=Expression();
		
		if(returnType!=BOOL){
			Error("Type incompatible");
		}
		
		cout << "\tpop %rax"<<endl;
		cout << "\tcmpq $0, %rax"<<endl;
		cout << "\tje FinWhile"<<tag<<endl;
		cout << "DO"<<tag<<":"<<endl;
		if(strcmp(lexer->YYText(), "DO")==0){
			current=(TOKEN) lexer->yylex();
			Statement();
			cout << "\tjmp WHILE"<<tag<<endl;
		}
		else{
			Error("'DO' attendu");
		}
		cout << "FinWhile"<<tag<<":"<<endl;
	}
	else{
			Error("'WHILE' attendu");
	}
}

void ForStatement(void){
	int tag, max;
	tag=++TagNumber;
	string variable;
	if(strcmp(lexer->YYText(), "FOR")==0){
		current=(TOKEN) lexer->yylex();
		cout << "FOR"<<tag<<":"<<endl;
		variable=lexer->YYText();
		AssignementStatement();
				
		cout << "TO"<<tag<<":"<<endl;
		if(strcmp(lexer->YYText(), "TO")==0){
			current=(TOKEN) lexer->yylex();
			max=current;
			Expression();
						
			cout << "DO"<<tag<<":"<<endl;
			if(strcmp(lexer->YYText(), "DO")==0){
				current=(TOKEN) lexer->yylex();
				cout << "\tpop %rbx" <<endl; //rbx = max
				cout << "\tmovq "<<variable<<", %rax"<<endl;
				cout << "\tcmpq %rbx, %rax"<<endl;
				cout << "\tja FinFor"<<tag<<endl;
				cout << "\tpush %rbx" <<endl;
				Statement();
				cout << "\tmovq "<<variable<<", %rax"<<endl;
				cout << "\taddq $1, %rax"<<endl;
				cout << "\tmovq %rax, "<<variable<<endl;
				cout << "\tjmp DO"<<tag<<endl;
			}
			
			else{
				Error("'DO' attendu");
			}
		}
		else{
			Error("'TO' attendu");
		}
		cout << "FinFor"<<tag<<":"<<endl;
	}
	else{
			Error("'WHILE' attendu");
	}
}

void BlockStatement(void){
	int tag;
	tag=++TagNumber;
	if(strcmp(lexer->YYText(), "BEGINN")==0){
		current=(TOKEN) lexer->yylex();
		cout << "BEGINN"<<tag<<":"<<endl;
		Statement();
		while(strcmp(lexer->YYText(), ";")==0){
			current=(TOKEN) lexer->yylex();
			Statement();
		}
		if(strcmp(lexer->YYText(), "END")==0){
			current=(TOKEN) lexer->yylex();
			cout << "END"<<tag<<":"<<endl;
		}
		else{
			Error("'END' attendu");
		}
		cout << "FinBeginn"<<tag<<":"<<endl;
	}
	else{
			Error("'BEGINN' attendu");
		}
}

void Display(void){
	int tag;
	tag=++TagNumber;  //peut avoir plusieurs display ?
	if(strcmp(lexer->YYText(), "DISPLAY")==0){
		current=(TOKEN) lexer->yylex();
		cout << "DISPLAY"<<tag<<":"<<endl;
		
		Expression();
		cout << "\tpop %rdx" << endl;              // The value to be displayed
		cout << "\tmovq $FormatString1, %rsi" << endl;   // "%llu\n"
		cout << "\tmovl    $1, %edi" << endl;
		cout << "\tmovl    $0, %eax" << endl;
		cout << "\tcall    __printf_chk@PLT" << endl;

	}
	else{
			Error("'DISPLAY' attendu");	
	}
}

// Statement := AssignementStatement
void Statement(void){
	
	if(strcmp(lexer->YYText(), "IF")==0) IfStatement();
	else if(strcmp(lexer->YYText(), "WHILE")==0) WhileStatement();
	else if(strcmp(lexer->YYText(), "FOR")==0) ForStatement();
	else if(strcmp(lexer->YYText(), "BEGINN")==0) BlockStatement(); 
	else if(strcmp(lexer->YYText(), "DISPLAY")==0) Display(); 
	else AssignementStatement();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==RBRACKET)
		DeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}

//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"

#include <string>
#include <iostream>
#include <cstdlib>

using namespace std;

//TP2:
// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"

char current, nextchar;
int NLookedAhead=0;  //nombre de caractere lu en avance

void ReadChar(void){
    if(NLookedAhead>0){
        current=nextchar;    // Char has already been read
        NLookedAhead--;
    }
    else
        // Read character and skip spaces until
        // non space character is read
        while(cin.get(current) && (current==' '||current=='\t'||current=='\n'));
}

void LookAhead(void){
    while(cin.get(nextchar) && (nextchar==' '||nextchar=='\t'||nextchar=='\n'));
    NLookedAhead++;
}

void Error(string s){
	cerr<< s << endl;
	exit(-1);
}

void AdditiveOperator(void){
	if(current=='+'||current=='-')
		ReadChar();
    else if (current=='|' && nextchar == '|'){
        ReadChar();
        ReadChar();
    }
	else
        if(current=='|'){
            Error(string("On a lu '")+current+string("' '")+nextchar+string("' Erreur : opérateur additif attendu"));
        }
        else
            Error(string("On a lu '")+current+string("' Erreur : opérateur additif attendu"));
            // Additive operator expected
}

void MultiplicativeOperator(void){
	if(current=='*'||current=='/'||current=='%')
		ReadChar();
    else if (current=='&' && nextchar == '&'){
        ReadChar();
        ReadChar();
    }
	else
        if(current=='&'){
            Error(string("On a lu '")+current+string("' '")+nextchar+string("' Erreur : opérateur multiplicatif attendu"));
        }
        else
            Error(string("On a lu '")+current+string("' Erreur : opérateur multiplicatif attendu"));
		// Multiplicative operator expected
}

void RelationnalOperator(void){
	if(current=='='){
		ReadChar();
	}
	else if(current=='<'){
		if(nextchar=='>'){
			ReadChar();
		}
		else if(nextchar=='='){
			ReadChar();
		}
		ReadChar();
	}
	else if(current=='>'){
		if(nextchar=='='){
			ReadChar();
		}
		ReadChar();
	}
	else{
		Error(string("On a lu '")+current+string("' Erreur : opérateur attendu"));
	}
}

void Digit(void){
	if((current<'0')||(current>'9'))
		Error(string("On a lu '")+current+string("' Erreur : digit attendu"));		   // Digit expected
	else{
		ReadChar();
	}
}

void Number(void){

    string tmpNumber;
    tmpNumber=current;
	Digit();
	while((current>='0') and (current<='9')){
        tmpNumber=tmpNumber+current;
		ReadChar();
	}
	cout << "\tpush "<<tmpNumber<<endl;
}

void Letter(void){
    ReadChar();
	if((current<'a')||(current>'z'))
		Error(string("On a lu '")+current+string("' Erreur : lettre attendue"));
}

void ArithmeticExpression();

void Expression();

void Factor(void){
    if (current>='0' and current<='9') {
        Number();
    }
    else if (current>='a' and current<='z') {
        //Letter();
        cout << "\tpush "<<current<<endl;
        ReadChar();
    }
    else if (current=='(') {
        ReadChar();
        Expression();
        if(current!=')'){
            Error(string("On a lu '")+current+string("' Erreur : ) attendu"));
        }
        else{
            ReadChar();
        }
    }
    else if (current=='!') {
        ReadChar();
        Factor();    //traitement de la factorielle a faire
    }
    else
        Error(string("On a lu '")+current+string("' Erreur : Factor attendu"));
}

void Term(void){
    char adop;
    Factor();
    //ReadChar();
    if(current=='*'||current=='/'||current=='%' || current=='&'){
        adop=current;
        MultiplicativeOperator();
        Factor();
        cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand

		if(adop=='*')
			cout << "\tmulq	%rbx, %rax"<<endl;	//
		else if(adop=='/')
			cout << "\tdivq	%rbx, %rax"<<endl;	//
        else if(adop=='%')
			cout << "\tdivq	%rbx, %rax"<<endl;	// // A AJOUTER L'OPERATION
        else if(adop=='&')
            cout << "\tdivq	%rbx, %rax"<<endl;	// A AJOUTER L'OPERATION &&

		cout << "\tpush %rax"<<endl;			// store result
    }
}

//= SimpleExpression
void ArithmeticExpression(void){ // Called by Term() and calls Term()
	char adop;
	Term();
	while(current=='+'||current=='-' || current=='|'){
		adop=current;		// Save operator in local variable
		if(current=='|'){
            LookAhead();
        }
		AdditiveOperator();
		Term();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand

		if(adop=='+')
			cout << "\taddq	%rbx, %rax"<<endl;	// add both operands
		else if(adop=='-')
			cout << "\tsubq	%rbx, %rax"<<endl;	// substract both operands
        else
            cout << "\tsubq	%rbx, %rax"<<endl;	// A AJOUTER L'OPERATION ||

		cout << "\tpush %rax"<<endl;			// store result
	}
}

void Expression(void){
	char adop,adop1;
	ArithmeticExpression();
	adop=current;		// Save operator in local variable
	LookAhead();
    adop1=nextchar;		// Save second character of operator in local variable

	if((current=='=')|| (current=='<') || (current=='>')){
        cout<<"current : "<<current<<" nextchar : "<<nextchar<<endl;
		RelationnalOperator();
        cout<<"current : "<<current<<" nextchar : "<<nextchar<<endl;
		ArithmeticExpression();
        cout<<"current : "<<current<<" nextchar : "<<nextchar<<endl;
		cout << "\tpop %rbx"<<endl;	// get second expression
		cout << "\tpop %rax"<<endl;	// get first expression
		cout << "\tcmpq %rax, %rbx"<<endl;

		if(adop=='='){
			cout << "\tje condvrai"<<endl;		// = equal
		}
		else if(adop=='<'){
		    if(adop1=='>'){
                	cout << "\tjne condvrai"<<endl;	   // <> not equal
		    }
		    else if(adop1=='='){
                	cout << "\tjbe condvrai"<<endl;	   // <= below or equal
		    }
		    else{
                	cout << "\tjb condvrai"<<endl;		// < below
		    }
		}
		else if(adop=='>'){
			if(adop1=='='){
                		cout << "\tjae condvrai"<<endl;	   // >= above or equal
		    	}
			else{
		        	cout << "\tja condvrai"<<endl;	   // > above
		    	}
		}
		cout << "\tpush $0"<<endl;
		cout << "\tjmp fincond"<<endl;
		cout << "condvrai:"<<endl;   //etiquette = pas de tab
		cout << "\tpush $0xFFFFFFFFFFFFFFFF"<<endl;
		cout << "fincond:"<<endl;
	}
}

void DeclarationPart(void){
    Letter();
    cout << "\t.data"<<endl;
    cout << current<<":\t.quad 0"<<endl;
	ReadChar();
	if(current!=',')
        Error(string("On a lu '")+current+string("' Erreur : ',' attendu"));
    while (current==','){
        Letter();
        cout << current<<":\t.quad 0"<<endl;
        ReadChar();
    }
 	if(current!=']')
        Error(string("On a lu '")+current+string("' Erreur : ']' attendu"));
}

void AssignementStatement(void){
    Letter();
    char tmpLetter=current;
	ReadChar();
	if(current!='=')
        Error(string("On a lu '")+current+string("' Erreur : '=' attendu"));
    ReadChar();
    Expression();
    cout << "\tpop "<<tmpLetter<<endl;
}

void Statement(void){
    AssignementStatement();
}

void StatementPart(void){
    cout << "\t.text"<<endl;
    Statement();
    if (current==';')
        Statement();
    if (current!='.')
        Error(string("On a lu '")+current+string("' Erreur : '.' attendu"));
}

void Program(void){
	ReadChar();
	if(current=='[')
        DeclarationPart();
    StatementPart();
}

int main(void){	// First version : Source code on standard input and assembly code on standard output

	// Header for gcc assembler / linker

	//freopen("test.p","r",stdin); //Pour lire dans un fichier 
	setlocale (LC_ALL,"");  // permet d'avoir les accents sous Windows avec encodage WINDOWS-1252

	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;

	// Let's proceed to the analysis and code production

	Program();
	//Expression();
	ReadChar();

	// Trailer for the gcc assembler / linker

	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(cin.get(current)){
		cerr <<"Caractères en trop à la fin du programme, on a lu : '"<<current<<"'";
		Error("."); // unexpected characters at the end of program
	}
}